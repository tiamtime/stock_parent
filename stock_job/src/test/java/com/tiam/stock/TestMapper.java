package com.tiam.stock;

import com.google.common.collect.Lists;
import com.tiam.stock.mapper.StockBusinessMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author : Tiam
 * @Date : 2024/5/13 11:00
 * @Description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestMapper {

    @Autowired
    private StockBusinessMapper stockBusinessMapper;

    /**
     * @desc 测试获取所有个股编码集合
     */
    @Test
    public void test01() {
        List<String> allCodes = stockBusinessMapper.getAllStockCodes();
        System.out.println(allCodes);
        //添加大盘业务前缀 sh sz
        allCodes = allCodes.stream().map(code -> code.startsWith("6") ? "sh" + code : "sz" + code).collect(Collectors.toList());
        System.out.println(allCodes);
        //将所有个股编码组成的大的集合拆分成若干小的集合 40 ---> 15 15 10
        Lists.partition(allCodes, 15).forEach(codes -> {
            System.out.println("size:" + codes.size() + ":" + codes);
        });
    }

}
