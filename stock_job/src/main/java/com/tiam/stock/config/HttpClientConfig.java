package com.tiam.stock.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @Author : Tiam
 * @Date : 2024/5/10 14:17
 * @Description : 定义http客户端工具bean
 */
@Configuration
public class HttpClientConfig {

    /**
     * 定义http客户端bean
     * @return
     */
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
