package com.tiam.stock.config;

import com.tiam.stock.pojo.vo.StockInfoConfig;
import com.tiam.stock.utils.IdWorker;
import com.tiam.stock.utils.ParserStockInfoUtil;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author : Tiam
 * @Date : 2024/5/6 14:20
 * @Description : 定义公共配置类
 */
@Configuration
@EnableConfigurationProperties({StockInfoConfig.class})//开启对象相关配置对象的加载
public class CommonConfig {

    /**
     * 基于雪花算法保证生成的id唯一
     * @return
     */
    @Bean
    public IdWorker idWorker() {
        /*
            参数1：机器ID
            参数2：机房ID
            机房和机器ID一般由运维人员进行唯一性规划
         */
        return new IdWorker(1l,2l);
    }

    /**
     * 定义解析 大盘 外盘 个股 板块相关信息的工具类bean
     * @param idWorker
     * @return
     */
    @Bean
    public ParserStockInfoUtil parserStockInfoUtil(IdWorker idWorker) {
        return new ParserStockInfoUtil(idWorker);
    }

}
