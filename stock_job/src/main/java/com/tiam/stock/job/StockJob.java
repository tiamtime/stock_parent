package com.tiam.stock.job;

import com.tiam.stock.service.StockTimerTaskService;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author : Tiam
 * @Date : 2024/6/24 10:11
 * @Description 定义xxl任务执行器bean
 */
@Component
public class StockJob {

    @Autowired
    private StockTimerTaskService stockTimerTaskService;

    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("myJobHandler")
    public void demoJobHandler() throws Exception {
        System.out.println("当前时间为：" + DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
    }

    /**
     * 定时采集A股大盘数据
     * 建议：针对不同的股票数据，定义不同的采集任务，解耦，方便维护
     */
    @XxlJob("getInnerMarketInfo")
    public void getStockInfos(){
        stockTimerTaskService.getInnerMarketInfo();
    }

    /**
     * 定时采集A股个盘数据
     */
    @XxlJob("getStockRtIndex")
    public void getStockRtIndex(){
        stockTimerTaskService.getStockRtIndex();
    }

    /**
     * 定时采集板块数据
     */
    @XxlJob("getPlateInfo")
    public void getPlateInfo(){
        stockTimerTaskService.getPlateInfo();
    }

}
