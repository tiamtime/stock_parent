package com.tiam.stock.mapper;

import com.tiam.stock.pojo.entity.SysRolePermission;

/**
* @author Tiam
* @description 针对表【sys_role_permission(角色权限表)】的数据库操作Mapper
* @createDate 2024-05-06 09:32:22
* @Entity com.tiam.stock.pojo.entity.SysRolePermission
*/
public interface SysRolePermissionMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysRolePermission record);

    int insertSelective(SysRolePermission record);

    SysRolePermission selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRolePermission record);

    int updateByPrimaryKey(SysRolePermission record);

}
