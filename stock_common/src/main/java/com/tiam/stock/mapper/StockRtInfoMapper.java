package com.tiam.stock.mapper;

import com.tiam.stock.pojo.domain.Stock4EvrDayDomain;
import com.tiam.stock.pojo.domain.Stock4MinuteDomain;
import com.tiam.stock.pojo.domain.StockUpdownDomain;
import com.tiam.stock.pojo.entity.StockRtInfo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Tiam
 * @description 针对表【stock_rt_info(个股详情信息表)】的数据库操作Mapper
 * @createDate 2024-05-06 09:32:22
 * @Entity com.tiam.stock.pojo.entity.StockRtInfo
 */
public interface StockRtInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockRtInfo record);

    int insertSelective(StockRtInfo record);

    StockRtInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockRtInfo record);

    int updateByPrimaryKey(StockRtInfo record);

    /**
     * 查询指定时间点下股票数据集合
     *
     * @param curDate 日期时间
     * @return
     */
    List<StockUpdownDomain> getStockInfoByTime(@Param("curDate") Date curDate);

    /**
     * 统计沪深两市个股最新交易数据，并按涨幅降序排序查询前4条数据
     *
     * @param curDate 日期时间
     * @return
     */
    List<StockUpdownDomain> getLatestData(@Param("curDate") Date curDate);

    /**
     * 统计指定日期范国内股票涨停或者跌停的数最流水
     *
     * @param startDate 开始时间，一般指开盘时间
     * @param endDate 截止时间
     * @param flag  flag约定：0代表跌停，1代表涨停
     * @return
     */
    List<Map> getStockUpdownCount(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("flag") int flag);

    /**
     * 统计指定时间点下股票在各个涨跌区间的数量
     *
     * @param curDate
     * @return
     */
    List<Map> getIncreaseRangeInfoByDate(@Param("dateTime") Date curDate);

    /**
     * 根据股票编码查询指定时间范围内的分时数据
     *
     * @param startDate 开盘时间
     * @param endDate 截止时间，一般与开盘时间一天
     * @param stockCode 股票编码
     * @return
     */
    List<Stock4MinuteDomain> getStock4MinuteInfo(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("stockCode") String stockCode);

    /**
     * 查询指定股票在指定日期范围内每天的最大时间
     *
     * @param startDate 开始时间
     * @param endDate 截止时间
     * @param stockCode 股票编码
     * @return
     */
    List<Date> getMaximumTime(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("stockCode") String stockCode);

    /**
     * 查询指定日期范围内指定股票每天的交易数据
     *
     * @param stockCode 股票编码
     * @param maximum 每天的最大时间集合
     * @return
     */
    List<Stock4EvrDayDomain> getStock4DkLine(@Param("stockCode") String stockCode, @Param("maximum") List<Date> maximum);

    /**
     * 批量插入个股数据
     *
     * @param list
     * @return
     */
    int insertBatch(@Param("list") List<StockRtInfo> list);
}
