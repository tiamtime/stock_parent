package com.tiam.stock.mapper;

import com.tiam.stock.pojo.entity.SysLog;

/**
* @author Tiam
* @description 针对表【sys_log(系统日志)】的数据库操作Mapper
* @createDate 2024-05-06 09:32:22
* @Entity com.tiam.stock.pojo.entity.SysLog
*/
public interface SysLogMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysLog record);

    int insertSelective(SysLog record);

    SysLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysLog record);

    int updateByPrimaryKey(SysLog record);

}
