package com.tiam.stock;

import com.tiam.stock.mapper.StockBusinessMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

/**
 * @Author : Tiam
 * @Date : 2024/5/6 14:25
 * @Description :
 */
@SpringBootTest
public class TestPasswordEncoder {

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * @desc 测试密码加密
     */
    @Test
    public void test01() {
        String pwd = "123456";
        String encodePwd = passwordEncoder.encode(pwd);
        //$2a$10$erJpM/u9/3Q9DpcTg0Sul.GMBvdzwiBKBQN/YeD7g9G5xI86ANfBK
        System.out.println(encodePwd);
    }

    /**
     * @desc 测试密码匹配
     */
    @Test
    public void test02() {
        String pwd = "123456";
        String enPwd = "$2a$10$erJpM/u9/3Q9DpcTg0Sul.GMBvdzwiBKBQN/YeD7g9G5xI86ANfBK";
        boolean isSuccess = passwordEncoder.matches(pwd, enPwd);
        System.out.println(isSuccess ? "密码匹配成功" : "密码匹配失败");
    }
}
