package com.tiam.stock.vo.req;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author : Tiam
 * @Date : 2024/5/6 11:35
 * @Description
 */
@Data
@ApiModel
public class LoginReqVo {
    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String username;
    /**
     * 密码
     */
    @ApiModelProperty("明文密码")
    private String password;
    /**
     * 验证码
     */
    @ApiModelProperty("验证码")
    private String code;
    /**
     * 会话ID
     */
    @ApiModelProperty("会话ID")
    private String sessionId;
}