package com.tiam.stock.mq;

import com.github.benmanes.caffeine.cache.Cache;
import com.tiam.stock.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author : Tiam
 * @Date : 2024/5/20 14:16
 * @Description 定义股票相关mq消息监听
 */
@Component
@Slf4j
public class StockMQMsgListener {

    @Autowired
    private Cache<String,Object> caffeineCache;

    @Autowired
    private StockService stockService;

    @RabbitListener(queues = "innerMarketQueue")
    public void refreshInnerMarketInfo(Date startTime) {
        //统计当前时间点与发送时间点的差值，如果超过1分钟，则告警
        //获取时间毫秒差值
        long diffTime = DateTime.now().getMillis() - new DateTime(startTime).getMillis();
        if (diffTime > 60000l) {
            log.error("大盘发送时间消息:{},延迟:{}ms",new DateTime(startTime).toString("yyyy-MM-dd HH:mm:ss"), diffTime);
        }
        //刷新缓存
        //删除旧的数据
        caffeineCache.invalidate("innerMarketKey");
        //调用服务方法，刷新数据
        stockService.getInnerMarketInfo();
    }

}
