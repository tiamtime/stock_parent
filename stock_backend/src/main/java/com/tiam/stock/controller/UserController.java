package com.tiam.stock.controller;

import com.tiam.stock.pojo.entity.SysUser;
import com.tiam.stock.service.UserService;
import com.tiam.stock.vo.req.LoginReqVo;
import com.tiam.stock.vo.resp.LoginRespVo;
import com.tiam.stock.vo.resp.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author : Tiam
 * @Date : 2024/5/6 10:33
 * @Description : 定义用户web层接口资源bean
 * 注意：@RequestBody接收json数据转化成实体对象:反序列化，@ResponseBody将实体对象转json格式字符串:序列化
 */
@RestController
@RequestMapping("/api")
@Api(tags = "用户相关接口处理器")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 根据用户名查询用户信息
     *
     * @param userName
     * @return
     */
    @ApiOperation(value = "根据用户名查询用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "用户名", dataType = "string", required = true, type = "path")
    })
    @GetMapping("/user/{userName}")
    public SysUser getUserByUserName(@PathVariable("userName") String userName) {
        return userService.findUserByUsername(userName);
    }

    /**
     * 用户登录功能
     *
     * @param vo
     * @return
     */
    @ApiOperation(value = "用户登录功能")
    @PostMapping("/login")
    public R<LoginRespVo> login(@RequestBody LoginReqVo vo) {
        return userService.login(vo);
    }

    /**
     * 生成图片验证码功能
     *
     * @return
     */
    @ApiOperation(value = "验证码生成")
    @GetMapping("/captcha")
    public R<Map> getCaptchaCode() {
        return userService.getCaptchaCode();
    }
}
