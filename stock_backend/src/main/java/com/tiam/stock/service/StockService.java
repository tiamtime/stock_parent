package com.tiam.stock.service;

import com.tiam.stock.pojo.domain.*;
import com.tiam.stock.vo.resp.PageResult;
import com.tiam.stock.vo.resp.R;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @Author : Tiam
 * @Date : 2024/5/7 17:16
 * @Description : 股票服务接口
 */
public interface StockService {

    /**
     * 获取国内大盘最新的数据
     *
     * @return
     */
    R<List<InnerMarketDomain>> getInnerMarketInfo();

    /**
     * 获取沪深两市板块最新数据，以交易总金额降序查询，取前10条数据
     *
     * @return
     */
    R<List<StockBlockDomain>> sectorAllLimit();

    /**
     * 分页查询最新的股票交易数据
     *
     * @param page
     * @param pageSize
     * @return
     */
    R<PageResult<StockUpdownDomain>> getStockInfoByPage(Integer page, Integer pageSize);

    /**
     * 统计沪深两市个股最新交易数据，并按涨幅降序排序查询前4条数据
     *
     * @return
     */
    R<List<StockUpdownDomain>> getLatestData();

    /**
     * 统计最新股票交易日内每分钟的涨跌停的股票数量
     *
     * @return
     */
    R<Map<String, List>> getStockUpdownCount();

    /**
     * 导出指定页码的最新股票数据
     *
     * @param page 当前页
     * @param pageSize 每页大小
     * @param response
     */
    void exportStockUpDownInfo(Integer page, Integer pageSize, HttpServletResponse response);

    /**
     * 统计大盘T日和T-1日且每分钟交易量的统计
     *
     * @return
     */
    R<Map<String, List>> getComparedStockTradeAmt();

    /**
     * 统计最新交易时间点下各个股票（A股）在各个涨幅区间的数量
     *
     * @return
     */
    R<Map> getIncreaseRangeInfo();

    /**
     * 获取指定股票T日的分时数据
     *
     * @param stockCode
     * @return
     */
    R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(String stockCode);

    /**
     * 统计指定股票的日k线
     *
     * @param stockCode 股票编码
     * @return
     */
    R<List<Stock4EvrDayDomain>> getStockScreenDKLine(String stockCode);
}
