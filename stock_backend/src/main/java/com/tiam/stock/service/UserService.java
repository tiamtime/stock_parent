package com.tiam.stock.service;

import com.tiam.stock.pojo.entity.SysUser;
import com.tiam.stock.vo.req.LoginReqVo;
import com.tiam.stock.vo.resp.LoginRespVo;
import com.tiam.stock.vo.resp.R;

import java.util.Map;

/**
 * @Author : Tiam
 * @Date : 2024/5/6 10:28
 * @Description : 定义用户服务接口
 */
public interface UserService {
    /**
     * 根据用户名查询用户信息
     * @param userName
     * @return
     */
    SysUser findUserByUsername(String userName);

    /**
     * 用户登录功能
     * @param vo
     * @return
     */
    R<LoginRespVo> login(LoginReqVo vo);

    /**
     * 生成图片验证码功能
     * @return
     */
    R<Map> getCaptchaCode();
}
