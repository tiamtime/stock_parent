package com.tiam.stock.service.impl;

import cn.hutool.core.lang.hash.Hash;
import com.alibaba.excel.EasyExcel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.tiam.stock.mapper.StockBlockRtInfoMapper;
import com.tiam.stock.mapper.StockMarketIndexInfoMapper;
import com.tiam.stock.mapper.StockRtInfoMapper;
import com.tiam.stock.pojo.domain.*;
import com.tiam.stock.pojo.vo.StockInfoConfig;
import com.tiam.stock.service.StockService;
import com.tiam.stock.vo.resp.PageResult;
import com.tiam.stock.vo.resp.R;
import com.tiam.stock.vo.resp.ResponseCode;
import com.tiam.stock.utils.DateTimeUtil;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author : Tiam
 * @Date : 2024/5/7 17:18
 * @Description : 股票服务实现
 */
@Service
@Slf4j
public class StockServiceImpl implements StockService {

    @Autowired
    private StockInfoConfig stockInfoConfig;

    @Autowired
    private StockMarketIndexInfoMapper stockMarketIndexInfoMapper;

    @Autowired
    private StockBlockRtInfoMapper stockBlockRtInfoMapper;

    @Autowired
    private StockRtInfoMapper stockRtInfoMapper;

    /**
     * 注入本地缓存bean
     */
    @Autowired
    private Cache<String,Object> caffeineCache;

    /**
     * 获取国内大盘最新的数据
     *
     * @return
     */
    @Override
    public R<List<InnerMarketDomain>> getInnerMarketInfo() {
        //默认从本地缓存假数据，如果不存在则从数据库加载并同步到本地缓存
        //在开盘周期内，本地缓存默认有效期1分钟
        R<List<InnerMarketDomain>> result = (R<List<InnerMarketDomain>>) caffeineCache.get("innerMarketKey", key -> {
            //1.获取股票最新的时间交易点（精确到分钟，秒和毫秒设置为0）
            Date curDate = DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
            //mock data 等后续完成股票采集job工程，再将代码删除即可
            curDate = DateTime.parse("2022-07-07 14:52:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
            //2.获取大盘编码集合
            List<String> mCodes = stockInfoConfig.getInner();
            //3.调用mapper查询数据
            List<InnerMarketDomain> data = stockMarketIndexInfoMapper.getMarketInfo(curDate, mCodes);
            //4.检验是否为空
            if (CollectionUtils.isEmpty(data)) {
                return R.error(ResponseCode.NO_RESPONSE_DATA.getMessage());
            }
            //5.封装并响应
            return R.ok(data);
        });
        return result;
    }

    /**
     * 获取沪深两市板块最新数据，以交易总金额降序查询，取前10条数据
     *
     * @return
     */
    @Override
    public R<List<StockBlockDomain>> sectorAllLimit() {
        //1.获取股票最新交易时间点（精确到分钟，秒和毫秒设置为0）
        Date curtDate = DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
        //mock data 等后续完成股票采集job工程，再将代码删除即可
        curtDate = DateTime.parse("2021-12-21 14:30:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        //2.调用mapper接口获取数据
        List<StockBlockDomain> infos = stockBlockRtInfoMapper.sectorAllLimit(curtDate);
        //3.校验是否为空
        if (CollectionUtils.isEmpty(infos)) {
            return R.error(ResponseCode.NO_RESPONSE_DATA.getMessage());
        }
        //4.封装并响应
        return R.ok(infos);
    }

    /**
     * 分页查询最新的股票交易数据
     *
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public R<PageResult<StockUpdownDomain>> getStockInfoByPage(Integer page, Integer pageSize) {
        //1.获取股票最新交易时间点（精确到分钟，秒和毫秒设置为0）
        Date curDate = DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
        //mock data 等后续完成股票采集job工程，再将代码删除即可
        curDate = DateTime.parse("2022-07-07 14:55:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        //2.设置PageHelper分页参数
        PageHelper.startPage(page, pageSize);
        //3.调用mapper查询数据
        List<StockUpdownDomain> pageData = stockRtInfoMapper.getStockInfoByTime(curDate);
        //4.组装PageResult对象
        PageInfo<StockUpdownDomain> pageInfo = new PageInfo<>(pageData);
        PageResult<StockUpdownDomain> pageResult = new PageResult<>(pageInfo);
        //5.响应数据
        return R.ok(pageResult);
    }

    /**
     * 统计沪深两市个股最新交易数据，并按涨幅降序排序查询前4条数据
     *
     * @return
     */
    @Override
    public R<List<StockUpdownDomain>> getLatestData() {
        //1.获取股票最新交易时间点（精确到分钟，秒和毫秒设置为0）
        Date curDate = DateTimeUtil.getLastDate4Stock(DateTime.now()).toDate();
        //mock data 等后续完成股票采集job工程，再将代码删除即可
        curDate = DateTime.parse("2022-07-07 14:55:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toDate();
        //2.调用mapper查询数据
        List<StockUpdownDomain> data = stockRtInfoMapper.getLatestData(curDate);
        //3.检验是否为空
        if (CollectionUtils.isEmpty(data)) {
            return R.error(ResponseCode.NO_RESPONSE_DATA.getMessage());
        }
        //4.响应数据
        return R.ok(data);
    }

    /**
     * 统计最新股票交易日内每分钟的涨跌停的股票数量
     *
     * @return
     */
    @Override
    public R<Map<String, List>> getStockUpdownCount() {
        //1.获取最新的股票交易时间点（截止时间）
        DateTime curDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        //假数据
        curDateTime = DateTime.parse("2022-01-06 14:25:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date endDate = curDateTime.toDate();
        //2.获取最新交易时间点对应的开盘时间点
        Date startDate = DateTimeUtil.getOpenDate(curDateTime).toDate();
        //3.统计涨停数据
        List<Map> upList = stockRtInfoMapper.getStockUpdownCount(startDate, endDate, 1);
        //4.统计跌停数据
        List<Map> downList = stockRtInfoMapper.getStockUpdownCount(startDate, endDate, 0);
        //5.组装数据
        HashMap<String, List> info = new HashMap<>();
        info.put("upList", upList);
        info.put("downList", downList);
        //6.响应数据
        return R.ok(info);
    }

    /**
     * 导出指定页码的最新股票数据
     *
     * @param page     当前页
     * @param pageSize 每页大小
     * @param response
     */
    @Override
    public void exportStockUpDownInfo(Integer page, Integer pageSize, HttpServletResponse response) {
        //1.获取分页数据
        R<PageResult<StockUpdownDomain>> r = this.getStockInfoByPage(page, pageSize);
        List<StockUpdownDomain> rows = r.getData().getRows();
        //2.将数据导出到excel中
        // 这里注意有同学反应使用swagger会导致各种问题，请直按用测览器或者用postman
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码，当然和easyexcel没有关系（默认名称）
        try {
            String fileName = URLEncoder.encode("股票信息表", "UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");
            EasyExcel
                    .write(response.getOutputStream(), StockUpdownDomain.class)
                    .sheet("股票涨幅信息")
                    .doWrite(rows);
        } catch (IOException e) {
            log.error("当前页码：{}，每页大小：{}，当前时间：{}，异常信息：{}", page, pageSize, DateTime.now().toString("yyyy-MM-dd HH:mm:ss"), e.getMessage());
            //通知前端异常，稍后重试
            response.setContentType("application/json");
            response.setCharacterEncoding("utf-8");
            R<Object> error = R.error(ResponseCode.ERROR);
            try {
                String jsonData = new ObjectMapper().writeValueAsString(error);
                response.getWriter().write(jsonData);
            } catch (IOException ex) {
                log.error("exportStockUpDownInfo:响应错误信息失败，时间：{}", DateTime.now().toString("yyyy-MM-dd HH:mm:ss"));
            }
        }
    }

    /**
     * 统计大盘T日和T-1日且每分钟交易量的统计
     *
     * @return
     */
    @Override
    public R<Map<String, List>> getComparedStockTradeAmt() {
        //1.获取T日（最新股票交易日的日期范围）
        DateTime tEndDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        //mock data
        tEndDateTime = DateTime.parse("2022-01-03 14:40:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date tEndDate = tEndDateTime.toDate();
        //开盘时间
        Date tStartDate = DateTimeUtil.getOpenDate(tEndDateTime).toDate();
        //2.获取T-1日的时间范围
        DateTime preTEndDateTime = DateTimeUtil.getPreviousTradingDay(tEndDateTime);
        //mock data
        preTEndDateTime = DateTime.parse("2022-01-02 14:40:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date preTEndDate = preTEndDateTime.toDate();
        //开盘时间
        Date preTStartDate = DateTimeUtil.getOpenDate(preTEndDateTime).toDate();
        //3.调用mapper查询
        //3.1 统计T日
        List<Map> tData = stockMarketIndexInfoMapper.getSumAmtInfo(tStartDate, tEndDate, stockInfoConfig.getInner());
        //3.2 统计T-1日
        List<Map> tPreData = stockMarketIndexInfoMapper.getSumAmtInfo(preTStartDate, preTEndDate, stockInfoConfig.getInner());
        //4.组装数据
        HashMap<String, List> info = new HashMap<>();
        info.put("amtList", tData);
        info.put("yesAmtList", tPreData);
        //5.响应数据
        return R.ok(info);
    }

    /**
     * 统计最新交易时间点下各个股票（A股）在各个涨幅区间的数量
     *
     * @return
     */
    @Override
    public R<Map> getIncreaseRangeInfo() {
        //1.获取当前最新的股票交易时间点
        DateTime curDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        //假数据
        curDateTime = DateTime.parse("2022-01-06 09:55:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date curDate = curDateTime.toDate();
        //2.调用mapper获取数据
        List<Map> infos = stockRtInfoMapper.getIncreaseRangeInfoByDate(curDate);
        //获取有序的涨幅区间标题集合
        List<String> upDownRange = stockInfoConfig.getUpDownRange();
        //将顺序的涨幅区间内的每个元素转换成Map对象即可
        //方式1：普通循环
//        List<Map> allInfos = new ArrayList<>();
//        for (String title : upDownRange) {
//            Map tmp = null;
//            for (Map info : infos) {
//                if (info.containsValue(title)) {
//                    tmp = info;
//                    break;
//                }
//            }
//            if (tmp == null) {
//                //不存在则补齐
//                tmp = new HashMap();
//                tmp.put("count", 0);
//                tmp.put("title", title);
//            }
//            allInfos.add(tmp);
//        }
        //方式2：stream遍历获取数据
        List<Map> allInfos = upDownRange.stream().map(title -> {
            Optional<Map> result = infos.stream().filter(map -> map.containsValue(title)).findFirst();
            if (result.isPresent()) {
                return result.get();
            } else {
                HashMap<String, Object> tmp = new HashMap<>();
                tmp.put("count", 0);
                tmp.put("title", title);
                return tmp;
            }
        }).collect(Collectors.toList());

        //3.组装数据
        HashMap<String, Object> data = new HashMap<>();
        data.put("time", curDateTime.toString("yyyy-MM-dd HH:mm:ss"));
//        data.put("infos", infos);
        data.put("infos", allInfos);
        //4.响应
        return R.ok(data);
    }

    /**
     * 获取指定股票T日的分时数据
     *
     * @param stockCode
     * @return
     */
    @Override
    public R<List<Stock4MinuteDomain>> getStockScreenTimeSharing(String stockCode) {
        //1.获取T日最新股票交易时间点 endDate startDate
        DateTime endDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        endDateTime = DateTime.parse("2021-12-30 14:30:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date endDate = endDateTime.toDate();
        Date startDate = DateTimeUtil.getOpenDate(endDateTime).toDate();
        //2.查询
        List<Stock4MinuteDomain> data = stockRtInfoMapper.getStock4MinuteInfo(startDate, endDate, stockCode);
        //判断非空处理
        if (CollectionUtils.isEmpty(data)) {
            data = new ArrayList<>();
        }
        //3.响应
        return R.ok(data);
    }

    /**
     * 统计指定股票的日k线
     *
     * @param stockCode 股票编码
     * @return
     */
    @Override
    public R<List<Stock4EvrDayDomain>> getStockScreenDKLine(String stockCode) {
        //1.获取统计日k线的数据时间范围
        //1.1 获取截止时间
        DateTime endDateTime = DateTimeUtil.getLastDate4Stock(DateTime.now());
        endDateTime = DateTime.parse("2022-01-06 14:25:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date endDate = endDateTime.toDate();
        //1.2 起始时间
        DateTime startDateTime = endDateTime.minusMonths(3);
        endDateTime = DateTime.parse("2022-01-01 09:30:00", DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss"));
        Date startDate = startDateTime.toDate();
        //2.调用mapper获取指定日期范围内的日k线数据
        //2.1 查询指定股票在指定日期范围内每天的最大时间，说白了就是以天分组，求每天最大时间
        List<Date> maximum = stockRtInfoMapper.getMaximumTime(startDate, endDate, stockCode);
        List<Stock4EvrDayDomain> dkLineData = stockRtInfoMapper.getStock4DkLine(stockCode, maximum);
        if (CollectionUtils.isEmpty(dkLineData)) {
            dkLineData = new ArrayList<>();
        }
        //3.响应
        return R.ok(dkLineData);
    }

}
